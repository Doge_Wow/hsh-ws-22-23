import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
import pickle
from nltk.corpus import stopwords
import nltk
nltk.download('stopwords')


imdb = pd.read_csv("https://drive.google.com/uc?id=1GHIOXS30WLVt_yYQ06wlxphcM8jhWykO")

train, test = train_test_split(imdb, test_size=0.2, stratify=imdb["sentiment"])

vectorizer = TfidfVectorizer(stop_words=stopwords.words('english'), max_features=500)
X = vectorizer.fit_transform(train["review"])

clf = LogisticRegression(random_state=42).fit(X, train["sentiment"])

X_test = vectorizer.transform(test["review"])

X_test_pred = clf.predict(X_test)

accuracy_score(test["sentiment"], X_test_pred)

with open("app/src/savedvectorizer.pickle", "wb") as f:
    pickle.dump(vectorizer, f)

with open("app/src/savedmodel.pickle", "wb") as f:
    pickle.dump(clf, f)