from typing import List
from fastapi import FastAPI
from pydantic import BaseModel
import pickle
import pandas as pd


with open('/exercises/ws20221015/savedmodel.pickle', 'rb') as f:
    loaded_model = pickle.load(f)

class DataIn(BaseModel):
    age: List[float]

class DataOut(BaseModel):
    prediction: List[float]

class Student(BaseModel):
    name: str
    alter: int
    studienfach: str

app = FastAPI()


@app.get("/")
async def root():
    return {"version": "0.0.1"}

@app.get("/name")
async def get_name(name: str = None):
    return {"name": name}

@app.post("/student", response_model=Student)
async def student(student: Student):
    return student

@app.post("/predict", response_model=DataOut)
def predict(data_in: DataIn):
    df = pd.DataFrame(data=data_in.dict())
    result = loaded_model.predict(df)
    return DataOut(prediction=list(result))