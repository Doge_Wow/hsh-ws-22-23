from typing import List
from fastapi import FastAPI
from pydantic import BaseModel
import pickle


with open('savedvectorizer.pickle', 'rb') as f:
    loaded_vectorizer = pickle.load(f)

with open('savedmodel.pickle', 'rb') as f:
    loaded_model = pickle.load(f)


class DataIn(BaseModel):
    review: List[str]

class DataOut(BaseModel):
    prediction: List[str]


app = FastAPI()


@app.post("/predict", response_model=DataOut)
def predict(data_in: DataIn):
    vectors = loaded_vectorizer.transform(data_in.review)
    pred = loaded_model.predict(vectors)
    return DataOut(prediction=pred.tolist())
