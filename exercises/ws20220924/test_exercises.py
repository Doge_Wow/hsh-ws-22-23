from datetime import datetime
import pytest

from .exercises import add, get_col_names, get_current_date_time, list_to_dict, split_words, sum_all_even_pos, sum_all_pos
import pandas as pd

d = {'col1': [1, 2], 'col2': [3, 4]}
df = pd.DataFrame(data=d)


@pytest.mark.parametrize('value_a,value_b,expected', [
    (
        1, 1, 2
    ),
    (
        -1, 1, 0
    ),

])
def test_add(value_a, value_b, expected):
    assert add(value_a, value_b) == expected


@pytest.mark.parametrize('string_in,expected', [
    (
        "Hallo ich bin Dennis", ["Hallo", "ich", "bin", "Dennis"]
    )
])
def test_split_words(string_in, expected):
    assert split_words(string_in) == expected


@pytest.mark.parametrize('df,expected', [
    (
        df, ["col1", "col2"]
    )
])
def test_col_names(df, expected):
    assert get_col_names(df) == expected


def test_get_current_date_time():
    datetime.strptime(get_current_date_time(), "%H:%M Uhr am %d.%m.%Y")
    assert True


@pytest.mark.parametrize('list_in,expected', [
    (
        [1,2,3], {1:1, 2:2, 3:3}
    ),
])
def test_list_to_dict(list_in, expected):
    assert list_to_dict(list_in) == expected


@pytest.mark.parametrize('int_in,expected', [
    (
        5, 15
    ),
    (
        100, 5050
    ),
])
def test_sum_all_pos(int_in, expected):
    assert sum_all_pos(int_in) == expected


@pytest.mark.parametrize('int_in,expected', [
    (
        5, 6
    ),
    (
        10, 30
    ),
])
def test_sum_all_even_pos(int_in, expected):
    assert sum_all_even_pos(int_in) == expected