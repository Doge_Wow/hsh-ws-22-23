## Some functions to practice

# Example function to add two values
from datetime import datetime
from typing import List
import pandas as pd

# TODO add two numbers and return the results
def add(a, b):
    return a+b

# TODO split input string on whitespaces and return a list of words
def split_words(string_in: str) -> List[str]:
    return string_in.split(" ")

# TODO receive a pandas data frame and return all column names as list
def get_col_names(df: pd.DataFrame) -> list:
    return list(df.columns)

# TODO return the current date and time in the form "14:05 Uhr am 24.09.2022"
def get_current_date_time() -> datetime:
    return datetime.now().strftime("%H:%M Uhr am %d.%m.%Y")

# TODO receive a list of n elements and return a dict in the form {1: elem1, 2: elem2, n: elemn}
def list_to_dict(list_in: list) -> dict:
    result = {}
    for i, element in enumerate(list_in):
        result[i+1] = element
    return result

# TODO receive an integer x and return the sum of all positiv integers from 0 to x
def sum_all_pos(int_in: int) -> int:
    if int_in == 0:
        return 0
    else:
        return int_in + sum_all_pos(int_in-1)

# TODO receive an integer y and return the sum of all even positiv integers from 0 to y
def sum_all_even_pos(int_in: int) -> int:
    if int_in == 0:
        return 0
    elif int_in % 2 != 0:
        return sum_all_even_pos(int_in-1)
    else:
        return int_in + sum_all_even_pos(int_in-1)


## Bonus
## use sklearn, numpy and pandas to train a first ML model (no need to write tests for this task for now)

# use the example dataset: https://scikit-learn.org/stable/modules/generated/sklearn.datasets.load_diabetes.html#sklearn.datasets.load_diabetes
# split the data into training and testing (80:20)
# fit a linear model to predict the target value using the age
# calculate the MSE of the model on the test set
